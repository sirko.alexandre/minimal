import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@minimal/api-interfaces';

@Component({
  selector: 'minimal-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  hello$ = this.http.get<Message>('/api/hello');
  users$ = this.http.get('/api/users');
  constructor(private http: HttpClient) {}
}
