import { MigrationInterface, QueryRunner } from 'typeorm';
import { User } from '../app/user.entity';

export class initUsers1647886137941 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(User, {
      firstName: 'a',
      lastName: 'a',
      isActive: true,
    });
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    //nothing
  }
}
