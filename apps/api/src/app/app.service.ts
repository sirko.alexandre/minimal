import { Injectable } from '@nestjs/common';
import { Message } from '@minimal/api-interfaces';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>
  ) {}

  getData(): Message {
    return { message: 'Welcome to api!' };
  }

  async getUsers() {
    return this.usersRepository.find();
  }
}
