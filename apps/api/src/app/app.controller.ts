import { Controller, Get } from '@nestjs/common';

import { Message } from '@minimal/api-interfaces';

import { AppService } from './app.service';
import { User } from './user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }
  @Get('users')
  getUsers(): Promise<User[]> {
    return this.appService.getUsers();
  }
}
