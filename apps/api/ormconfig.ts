import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: +process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
  entities: [__dirname + '/**/*.entity.ts'],
  migrations: [__dirname + '/src/migrations/**/*.ts'],
  cli: {
    migrationsDir: 'apps/api/src/migrations',
  },
  ssl: false,
  synchronize: true,
  autoLoadEntities: true,
  logging: false,
  // logger: 'advanced-console',
} as TypeOrmModuleOptions;
