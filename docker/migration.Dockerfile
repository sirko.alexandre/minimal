FROM node:16
WORKDIR /opt/back
COPY . .
RUN npm ci --cache .npm --prefer-offline
RUN npm run build:back

EXPOSE 3000

CMD ["npm" ,"run" ,"start:back:prod"]
