FROM node:16 as builder
WORKDIR /opt/back
COPY . .
RUN npm ci --cache .npm --prefer-offline
RUN npm run build:back

FROM node:16
WORKDIR /opt/back
EXPOSE 3000
COPY --from=builder /opt/back/dist/apps/api /opt/back/dist/apps/api
COPY --from=builder /opt/back/*.json /opt/back/
COPY --from=builder /opt/back/node_modules /opt/back/node_modules
# faster to copy node module than reinstall in prod mode
#RUN npm i --production
CMD ["npm" ,"run" ,"start:back:prod"]
