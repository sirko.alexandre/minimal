FROM node:16 as builder
WORKDIR /opt/front
COPY . .
RUN npm ci --cache .npm --prefer-offline
RUN npm run build:front -- --prod

FROM nginx:1.17-alpine
RUN rm -rf /usr/share/nginx/html/*

COPY ./docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/vhost.conf /etc/nginx/conf.d/default.conf
COPY ./docker/nginx/cache.conf /etc/nginx/conf.d/cache.conf
COPY ./docker/nginx/default_location.conf /etc/nginx/location.d/default.conf

COPY --from=builder --chown=nginx:nginx /opt/front/dist/apps/minimal /usr/share/nginx/html

WORKDIR /usr/share/nginx/html
